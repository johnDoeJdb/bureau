<?php

namespace AppBundle\Model;

class SocialButtons
{
    protected $facebook;

    protected $vkontakte;

    protected $instagram;

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return mixed
     */
    public function getVkontakte()
    {
        return $this->vkontakte;
    }

    /**
     * @param mixed $vkontakte
     */
    public function setVkontakte($vkontakte)
    {
        $this->vkontakte = $vkontakte;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param mixed $instagram
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }
}
