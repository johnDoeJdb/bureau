<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Hip\MandrillBundle\Message;
use Hip\MandrillBundle\Dispatcher;

class Mailer
{
    protected $container;

    /**
     * Mailer constructor.
     * @param $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function sendMail($email, $subject, $message)
    {
        $dispatcher = $this->container->get('hip_mandrill.dispatcher');
        $message = new Message();
        $message
            ->setFromEmail('no-reply@burovsego.ru')
            ->setFromName('Burovsego mailing system')
            ->addTo($email)
            ->setSubject($subject)
            ->setHtml($message);
        $dispatcher->send($message);
    }
}
