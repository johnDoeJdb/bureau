<?php

namespace AppBundle\Services;

use AppBundle\Entity\Payment;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Gos\Component\WebSocketClient\Wamp\Client;

class PaymentManager
{
    protected $container;

    /**
     * Mailer constructor.
     * @param $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function generateXmlRequest($amount, $discussionId)
    {
        $paymentSystem = $this->container->get('doctrine')->getRepository('AppBundle:PaymentSystem')->findOneBy(array());
        $discussion = $this->container->get('doctrine')->getRepository('AppBundle:Discussion')->findOneBy(array('id' => $discussionId));
        $router = $this->container->get('router');
        $successUrl = $router->generate('payment_success', array(), true);
        $failUrl = $router->generate('payment_fail', array(), true);
        $resultUrl = $router->generate('payment_result', array(), true);
        $testMode = $paymentSystem->getTestMode() ? 1 : 0;
        $externalId = time();

        $domtree = new \DOMDocument('1.0', 'UTF-8');
        $xmlRoot = $domtree->createElement("request");
        /* append it to the document created */
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $xmlRoot->appendChild($domtree->createElement('version','1.3'));
        $xmlRoot->appendChild($domtree->createElement('merchant_id', $paymentSystem->getMerchantId()));
        $xmlRoot->appendChild($domtree->createElement('language','ru'));
        $xmlRoot->appendChild($domtree->createElement('order_id', $externalId));
        $xmlRoot->appendChild($domtree->createElement('amount', $amount));
        $xmlRoot->appendChild($domtree->createElement('currency', 'RUB'));
        $xmlRoot->appendChild($domtree->createElement('description','?????? ?? ????? burovsego.ru'));
        $success_url = $xmlRoot->appendChild($domtree->createElement('success_url'));
        $success_url->appendChild($domtree->createCDATASection($successUrl));
        $fail_url = $xmlRoot->appendChild($domtree->createElement('fail_url'));
        $fail_url->appendChild($domtree->createCDATASection($failUrl));
        $result_url = $xmlRoot->appendChild($domtree->createElement('result_url'));
        $result_url->appendChild($domtree->createCDATASection($resultUrl));
        $xmlRoot->appendChild($domtree->createElement('test_mode', $testMode));
        $other = $xmlRoot->appendChild($domtree->createElement('other'));
        $other->appendChild($domtree->createCDATASection($discussionId));

        $payment = new Payment();
        $payment->setAmount($amount);
        $payment->setCompleted(false);
        $payment->setExternalId($externalId);
        $payment->setTimestamp($externalId);
        $payment->setDiscussion($discussion);

        $this->container->get('doctrine')->getManager()->persist($payment);
        $this->container->get('doctrine')->getManager()->flush();

        return $domtree->saveXML();
    }

    public function checkXmlResponse($xml, $sign)
    {
        $response = new \stdClass();
        $response->status = false;
        $paymentSystem = $this->container->get('doctrine')->getRepository('AppBundle:PaymentSystem')->findOneBy(array());
        if (!$sign || !$xml) {
            $response->message = 'Не получен ответ от платежной системы. Пожалуйста, повторите попытку оплаты.';
            return $response;
        }
        $xml = base64_decode(str_replace(' ', '+', $xml));
        $sign = base64_decode(str_replace(' ', '+', $sign));
        if (!$sign || !$xml) {
            $response->message = 'Получены некорректные данные. Пожалуйста, повторите попытку оплаты.';
            return $response;
        }

        $xml = simplexml_load_string($xml);
        $orderId = (integer) $xml->order_id;
        $status = (string) $xml->status;
        $generatedSign = md5($paymentSystem->getHiddenKey().$xml.$paymentSystem->getHiddenKey());
        if ($orderId && $status == 'success' && $generatedSign == $generatedSign) {
            $discussionId = (string) $xml->other;
            $discussion = $this->container->get('doctrine')->getRepository('AppBundle:Discussion')->findOneBy(array('id' => $discussionId));
            $payment = $this->container->get('doctrine')->getRepository('AppBundle:Payment')->findOneBy(array(
                'discussion' => $discussion,
                'externalId' => (string) $orderId,
                'amount'     => (float) $xml->amount,
            ));

            if ($payment) {
                $response->status = true;
                if ($payment->getCompleted()) {
                    $response->message = 'Счет уже был оплачен'."<script>window.close()</script>";
                    return $response;
                } else {
                    $payment->setCompleted(true);
                    $this->container->get('doctrine')->getManager()->persist($payment);
                    $this->container->get('doctrine')->getManager()->flush();

                    $client = new Client('127.0.0.1', '81');
                    $client->connect();
                    $client->publish('chat#'.$discussionId, json_encode(array('text'=>'Заказ оплачен', 'type' => 'payed')));
                    $client->publish('service_chat#'.$discussionId, json_encode(array('type' => 'payment')));
                    $client->disconnect();

                    $response->message = 'Оплата проведена успешно';
                    return $response;
                }
            } else {
                $response->message = 'Некорректная цифрая подпись или некорректный статус оплаты. Пожалуйста, повторите попытку оплаты.';
                return $response;
            }
        }

        $response->message = 'Некорректная цифрая подпись или некорректный статус оплаты. Пожалуйста, повторите попытку оплаты.';
        return $response;
    }

    public function generateXmlResponse($status, $message)
    {
        $domtree = new \DOMDocument('1.0', 'UTF-8');
        $xmlRoot = $domtree->createElement("result");
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $xmlRoot->appendChild($domtree->createElement('status', $status ? 'yes' : 'no' ));
        $xmlRoot->appendChild($domtree->createElement('error_msg',  $status ? $message : '' ));

        return $domtree->saveXML();
    }

    public function generateSign($xml)
    {
        $paymentSystem = $this->container->get('doctrine')->getRepository('AppBundle:PaymentSystem')->findOneBy(array());

        return  md5($paymentSystem->getSecretKey().$xml.$paymentSystem->getSecretKey());
    }
}
