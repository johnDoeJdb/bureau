<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MenuItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('link', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('menuType', 'choice', array(
                'choices' => array(
                    'top_menu' => 'Верхнее меню',
                    'bottom_menu' => 'Нижнее меню',
                ),
                'error_bubbling' => true,
            ))
            ->add('sortOrder', 'integer', array(
                'required' => false,
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'menu_item';
    }
}