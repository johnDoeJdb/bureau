<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('zip', 'number', array(
                'error_bubbling' => true,
            ))
            ->add('address', 'textarea', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'address';
    }
}