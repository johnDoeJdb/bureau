<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SnippetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('context', 'textarea', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'snippet';
    }
}