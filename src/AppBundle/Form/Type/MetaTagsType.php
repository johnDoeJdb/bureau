<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MetaTagsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('property', 'text', array(
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('content', 'text', array(
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('path', 'text', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'meta_tag';
    }
}