<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class VacancyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('description', 'textarea', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'vacancy';
    }
}