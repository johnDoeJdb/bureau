<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SubmitBusinessType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('area', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('company_url', 'text', array(
                'required'      => false,
                'error_bubbling' => true,
            ))
            ->add('person', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('phone', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('email', 'text', array(
                'required'      => false,
                'error_bubbling' => true,
            ))
            ->add('comments', 'text', array(
                'required' => false,
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'submit_business';
    }
}