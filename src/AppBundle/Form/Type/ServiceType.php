<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('chatPlaceholder', 'textarea', array(
                'error_bubbling' => true,
            ))
            ->add('imageFile', 'vich_file', array(
                'required'      => false,
                'allow_delete'      => false,
                'download_link'      => false,
            ));
        ;
    }

    public function getName()
    {
        return 'service';
    }
}