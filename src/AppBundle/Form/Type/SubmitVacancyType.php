<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SubmitVacancyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('vacancy', 'entity', array(
                'class' => 'AppBundle\Entity\Vacancy',
                'property' => 'position',
                'empty_value' => '-- Выберете вакансию --'
            ))
            ->add('fullName', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('phone', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('email', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('comments', 'text', array(
                'required'      => false,
                'error_bubbling' => true,
            ))
            ->add('file', 'file', array(
                'attr'      => array(
                    'class' => 'e-request-form-requirements__content-position'
                ),
            ));
        ;
    }

    public function getName()
    {
        return 'submit_vacancy';
    }
}