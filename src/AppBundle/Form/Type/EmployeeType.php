<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('position', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('phone', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('email', 'text', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'employee';
    }
}