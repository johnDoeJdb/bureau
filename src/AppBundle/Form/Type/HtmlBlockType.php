<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HtmlBlockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', 'textarea', array(
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('onEachPage', 'checkbox', array(
                'required' => false,
                'error_bubbling' => true,
            ))
            ->add('place', 'choice', array(
                'choices' => array(
                    'header' => 'header',
                    'body' => 'body',
                ),
                'error_bubbling' => true,
            ))
            ->add('content', 'textarea', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'html_block';
    }
}