<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('email', 'email', array(
                'error_bubbling' => true,
            ))
            ->add('fullName', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('roles', 'choice', array(
                'choices' => array(
                    'ROLE_ADMIN' => "Administrator",
                    'ROLE_OPERATOR' => "Operator",
                ),
                'multiple' => true,
                'error_bubbling' => true,
            ))
            ->add('password', 'password', array(
                'error_bubbling' => true,
            ))
            ->add('file', 'file',  array(
                'required' => false,
                'error_bubbling' => true,
            ));
        ;
    }

    public function getName()
    {
        return 'user';
    }
}