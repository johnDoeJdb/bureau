<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SocialButtonsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facebook', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('vkontakte', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('instagram', 'text', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'social_buttons';
    }
}