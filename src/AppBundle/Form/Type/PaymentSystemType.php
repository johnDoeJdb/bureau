<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PaymentSystemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('merchantId', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('secretKey', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('hiddenKey', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('apiKey', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('siteUrl', 'text', array(
                'error_bubbling' => true,
            ))
            ->add('testMode', 'checkbox', array(
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'payment_system';
    }
}