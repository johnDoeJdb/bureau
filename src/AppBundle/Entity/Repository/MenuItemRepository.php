<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class MenuItemRepository extends EntityRepository
{
    public function getTopMenuItems()
    {
        return $this->createQueryBuilder('m')
            ->select('')
            ->where('m.menuType = :type')->setParameter('type', 'top_menu')
            ->orderBy('m.sortOrder')
            ->getQuery()
            ->getResult();
    }

    public function getBottomMenuItems()
    {
        return $this->createQueryBuilder('m')
            ->select('m.title, m.link, m.id')
            ->where('m.menuType = :type')->setParameter('type', 'bottom_menu')
            ->orderBy('m.sortOrder')
            ->getQuery()
            ->getArrayResult();
    }
}