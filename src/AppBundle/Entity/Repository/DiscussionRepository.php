<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DiscussionRepository extends EntityRepository
{
    public function getDiscussionByTypeQuery($type, $operatorId = null)
    {
        $qb = $this->createQueryBuilder('d')->select();
        if ($type == 'new') {
            $qb->where('d.isActiveOperator is NULL');
        } else if ($type == 'my') {
            $qb->leftJoin('d.operator', 'operator')
                ->where('operator.id = :operatorId')->setParameter('operatorId', $operatorId)
                ->andWhere('d.isActiveClient = true')
                ->andWhere('d.timestamp > :date')->setParameter('date', (new \DateTime())->modify('- 1 day')->getTimestamp());
        } else if ($type == 'archive') {
            $qb->where('d.client is NOT NULL OR d.operator is NOT NULL')
               ->andWhere('d.isActiveClient = false')
               ->andWhere('d.isActiveOperator = false');
        } else if ($type == 'missed') {
            $qb->leftJoin('d.client', 'client')
               ->where('d.client is NOT NULL')
               ->andWhere('d.operator is NULL')
               ->andWhere('d.isActiveClient = false')
               ->andWhere('d.isActiveOperator is NULL')
               ->andWhere('client.email is NOT NULL OR client.phone is NOT NULL');
        }

        return $qb
                ->orderBy('d.id', 'DESC')
                ->getQuery();
    }

    public function getNumberOfNewDiscussions()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d)')
            ->where('d.isActiveOperator is NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getHangedDiscussions()
    {
        return $this->createQueryBuilder('d')
            ->select('')
            ->where('d.timestamp = :date')->setParameter('date', (new \DateTime())->modify('-1 hour')->getTimestamp())
            ->andWhere('d.isActiveClient = true')
            ->getQuery()
            ->getResult();
    }

    public function getNextDiscussions($timestamp)
    {
        return $this->createQueryBuilder('d')
            ->select('d.id, d.timestamp')
            ->where('d.timestamp > :timestamp')->setParameter('timestamp', $timestamp)
            ->getQuery()
            ->getResult();
    }
}