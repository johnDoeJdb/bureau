<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function getMessagesBydDiscussion($discussionId)
    {
        return $this->createQueryBuilder('m')
            ->select()
            ->leftJoin('m.discussion', 'd')
            ->where('d.id = :discussionId')
            ->setParameter('discussionId', $discussionId)
            ->orderBy('m.timestamp', 'ASC')
            ->getQuery()
            ->getResult();
    }
}