<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class HtmlBlockRepository extends EntityRepository
{
    public function getHtmlBlocks($path, $place)
    {
        return $this->createQueryBuilder('d')
            ->select('')
            ->where('d.path = :path OR d.onEachPage = :eachPage')->setParameter('path', $path)->setParameter('eachPage', true)
            ->andWhere('d.place = :place')->setParameter('place', $place)
            ->getQuery()
            ->getResult();
    }
}