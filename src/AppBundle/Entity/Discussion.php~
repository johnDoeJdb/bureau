<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="discussions")+
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\DiscussionRepository")
 */
class Discussion
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="discussions")
     * @ORM\JoinColumn(name="operator_id", referencedColumnName="id")
     */
    protected $operator;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="discussions")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment", mappedBy="discussion")
     **/
    protected $payments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Message", mappedBy="discussion")
     **/
    protected $messages;

    /**
     * @ORM\Column(type="bigint")
     */
    protected $timestamp;

    /**
     * @ORM\Column(type="boolean", name="is_active_client", nullable=true)
     */
    protected $isActiveClient;

    /**
     * @ORM\Column(type="boolean", name="is_active_operator", nullable=true)
     */
    protected $isActiveOperator;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param integer $timestamp
     * @return Discussion
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return integer 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set operator
     *
     * @param \AppBundle\Entity\User $operator
     * @return Discussion
     */
    public function setOperator(\AppBundle\Entity\User $operator = null)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator
     *
     * @return \AppBundle\Entity\User 
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return Discussion
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add payments
     *
     * @param \AppBundle\Entity\Payment $payments
     * @return Discussion
     */
    public function addPayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \AppBundle\Entity\Payment $payments
     */
    public function removePayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * Add messages
     *
     * @param \AppBundle\Entity\Message $messages
     * @return Discussion
     */
    public function addMessage(\AppBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \AppBundle\Entity\Message $messages
     */
    public function removeMessage(\AppBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set isActiveClient
     *
     * @param boolean $isActiveClient
     * @return Discussion
     */
    public function setIsActiveClient($isActiveClient)
    {
        $this->isActiveClient = $isActiveClient;

        return $this;
    }

    /**
     * Get isActiveClient
     *
     * @return boolean 
     */
    public function getIsActiveClient()
    {
        return $this->isActiveClient;
    }

    /**
     * Set isActiveOperator
     *
     * @param boolean $isActiveOperator
     * @return Discussion
     */
    public function setIsActiveOperator($isActiveOperator)
    {
        $this->isActiveOperator = $isActiveOperator;

        return $this;
    }

    /**
     * Get isActiveOperator
     *
     * @return boolean 
     */
    public function getIsActiveOperator()
    {
        return $this->isActiveOperator;
    }
}
