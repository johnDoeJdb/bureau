<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="payments")
 */
class Payment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="bigint", name="external_id")
     */
    protected $externalId;

    /**
     * @ORM\Column(type="float", name="amount")
     */
    protected $amount;

    /**
     * @ORM\Column(type="string", name="description", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", name="completed")
     */
    protected $completed;

    /**
     * @ORM\Column(type="integer", name="timestamp")
     */
    protected $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discussion", inversedBy="payments")
     * @ORM\JoinColumn(name="discussion_id", referencedColumnName="id")
     */
    protected $discussion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     * @return Payment
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Payment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     * @return Payment
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return boolean 
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set timestamp
     *
     * @param integer $timestamp
     * @return Payment
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return integer 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set discussion
     *
     * @param \AppBundle\Entity\Discussion $discussion
     * @return Payment
     */
    public function setDiscussion(\AppBundle\Entity\Discussion $discussion = null)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return \AppBundle\Entity\Discussion 
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }
}
