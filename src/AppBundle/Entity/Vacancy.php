<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="vacancies")
 */
class Vacancy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", name="position")
     **/
    protected $position;

    /**
     * @ORM\Column(type="text", name="description")
     **/
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SubmitVacancy", mappedBy="vacancy")
     **/
    protected $vacancy;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Vacancy
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Vacancy
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vacancy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vacancy
     *
     * @param \AppBundle\Entity\SubmitVacancy $vacancy
     * @return Vacancy
     */
    public function addVacancy(\AppBundle\Entity\SubmitVacancy $vacancy)
    {
        $this->vacancy[] = $vacancy;

        return $this;
    }

    /**
     * Remove vacancy
     *
     * @param \AppBundle\Entity\SubmitVacancy $vacancy
     */
    public function removeVacancy(\AppBundle\Entity\SubmitVacancy $vacancy)
    {
        $this->vacancy->removeElement($vacancy);
    }

    /**
     * Get vacancy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVacancy()
    {
        return $this->vacancy;
    }
}
