<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="html_blocks")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\HtmlBlockRepository")
 */
class HtmlBlock
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="place")
     */
    protected $place;

    /**
     * @ORM\Column(type="boolean", name="on_each_page")
     */
    protected $onEachPage;

    /**
     * @ORM\Column(type="string", name="path", nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(type="text", name="content")
     */
    protected $content;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param string $place
     * @return HtmlInjections
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return HtmlInjections
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return HtmlInjections
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set onEachPage
     *
     * @param boolean $onEachPage
     * @return HtmlBlock
     */
    public function setOnEachPage($onEachPage)
    {
        $this->onEachPage = $onEachPage;

        return $this;
    }

    /**
     * Get onEachPage
     *
     * @return boolean 
     */
    public function getOnEachPage()
    {
        return $this->onEachPage;
    }
}
