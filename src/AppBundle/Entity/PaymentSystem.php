<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment_system")
 */
class PaymentSystem
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", name="merchant_id")
     */
    protected $merchantId;

    /**
     * @ORM\Column(type="string", name="secret_key")
     */
    protected $secretKey;

    /**
     * @ORM\Column(type="string", name="hidden_key")
     */
    protected $hiddenKey;


    /**
     * @ORM\Column(type="string", name="api_key")
     */
    protected $apiKey;

    /**
     * @ORM\Column(type="string", name="site_url")
     */
    protected $siteUrl;

    /**
     * @ORM\Column(type="boolean", name="test_mode")
     */
    protected $testMode;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set merchantId
     *
     * @param integer $merchantId
     * @return PaymentSystem
     */
    public function setMerchantId($merchantId)
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    /**
     * Get merchantId
     *
     * @return integer 
     */
    public function getMerchantId()
    {
        return $this->merchantId;
    }

    /**
     * Set secretKey
     *
     * @param string $secretKey
     * @return PaymentSystem
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;

        return $this;
    }

    /**
     * Get secretKey
     *
     * @return string 
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * Set hiddenKey
     *
     * @param string $hiddenKey
     * @return PaymentSystem
     */
    public function setHiddenKey($hiddenKey)
    {
        $this->hiddenKey = $hiddenKey;

        return $this;
    }

    /**
     * Get hiddenKey
     *
     * @return string 
     */
    public function getHiddenKey()
    {
        return $this->hiddenKey;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return PaymentSystem
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set siteUrl
     *
     * @param string $siteUrl
     * @return PaymentSystem
     */
    public function setSiteUrl($siteUrl)
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    /**
     * Get siteUrl
     *
     * @return string 
     */
    public function getSiteUrl()
    {
        return $this->siteUrl;
    }

    /**
     * Set testMode
     *
     * @param boolean $testMode
     * @return PaymentSystem
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * Get testMode
     *
     * @return boolean 
     */
    public function getTestMode()
    {
        return $this->testMode;
    }
}
