<?php

namespace AppBundle\Command;

use AppBundle\WebSocket\Chat;
use Ratchet\Wamp\WampServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class WebSocketCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('websocket:start')
            ->setDescription('Start WebSocket server')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $this->getContainer()->get('app.chat')
                    )
                )
            )
            , 81
        );
        $server->run();

    }
}