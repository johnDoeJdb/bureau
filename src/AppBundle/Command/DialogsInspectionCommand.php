<?php

namespace AppBundle\Command;

use AppBundle\WebSocket\Chat;
use Ratchet\Wamp\WampServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class DialogsInspectionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dialogs:clean')
            ->setDescription('Remove all hanged chats')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $hangedDialogs = $doctrine->getRepository('AppBundle:Discussion')->getHangedDiscussions();
        $manager = $doctrine->getManager();
        foreach ($hangedDialogs as $dialog) {
            $manager->remove($dialog);
            $manager->flush();
        }
    }
}