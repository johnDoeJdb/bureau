<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\SubmitBusiness;
use AppBundle\Entity\SubmitVacancy;
use AppBundle\Form\Type\SubmitBusinessType;
use AppBundle\Form\Type\SubmitVacancyType;
use Proxies\__CG__\AppBundle\Entity\MobileApplicationRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Template()
     * @Cache(smaxage="3600", maxage="3600", public=true)
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $orders  = $this->getDoctrine()->getRepository('AppBundle:Order')->findAll();
        $services  = $this->getDoctrine()->getRepository('AppBundle:Service')->findAll();

        return array('services' => $services, 'orders' => $orders);
    }

    /**
     * @Template()
     * @Cache(smaxage="3600", maxage="3600", public=true)
     * @Route("/contacts", name="contacts")
     */
    public function contactsAction()
    {
        $addresses = $this->getDoctrine()->getRepository('AppBundle:Address')->findAll();
        $employees = $this->getDoctrine()->getRepository('AppBundle:Employee')->findAll();
        if ($addresses) {
            $address = $addresses[0];
        } else {
            $address = new Address();
        }

        return array('address' => $address, 'employees' => $employees);
    }

    /**
     * @Template()
     * @Cache(smaxage="3600", maxage="3600", public=true)
     * @Route("/business", name="business")
     */
    public function businessAction()
    {
        $submitBusiness = new SubmitBusiness();
        $form = $this->createForm(new SubmitBusinessType(), $submitBusiness);

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Cache(smaxage="3600", maxage="3600", public=true)
     * @Route("/career", name="career")
     */
    public function careerAction()
    {
        $submitVacancy = new SubmitVacancy();
        $vacancies = $this->getDoctrine()->getRepository('AppBundle:Vacancy')->findAll();
        $form = $this->createForm(new SubmitVacancyType(), $submitVacancy);

        return array('vacancies' => $vacancies, 'form' => $form->createView());
    }

    /**
     * @Cache(smaxage="3600", maxage="3600", public=true)
     * @Route("/request-mobile-application", name="request_mobile_application")
     */
    public function requestAction(Request $request)
    {
        $platform = $request->request->get('platform');
        $email = $request->request->get('email');
        $mobileRequest = new MobileApplicationRequest();
        $mobileRequest->setEmail($email);
        $mobileRequest->setType($platform);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($mobileRequest);
        $entityManager->flush();

        return new JsonResponse( ['status' => 'ok']);
    }

    /**
     * @Route("/vacancy/submit", name="submit_vacancy")
     */
    public function submitVacancyAction(Request $request)
    {
        $submitVacancy = new SubmitVacancy();
        $form = $this->createForm(new SubmitVacancyType(), $submitVacancy);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $submitVacancy = $form->getData();
            if ($submitVacancy->getFile()) {
                $submitVacancy->upload($submitVacancy->getEmail());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($submitVacancy);
            $entityManager->flush();
        }

        return $this->redirectToRoute('career');
    }

    /**
     * @Route("/business/submit", name="submit_business")
     */
    public function submitBusinessAction(Request $request)
    {
        $submitBusiness = new SubmitBusiness();
        $form = $this->createForm(new SubmitBusinessType(), $submitBusiness);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $submitBusiness = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($submitBusiness);
            $entityManager->flush();
        }

        return $this->redirectToRoute('business');
    }

    /**
     * @Route("/ping/", name="ping_request")
     */
    public function pingAction()
    {
        $user = $this->getUser();
        if ($user) {
            $manager = $this->getDoctrine()->getManager();
            $user->setLastActivity(new \DateTime());
            $manager->persist($user);
            $manager->flush();
        }

        return new JsonResponse(['status' => 'ok']);
    }
}
