<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Output;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

class BaseController extends Controller
{
    protected function execute($command)
    {
        $app = new Application($this->get('kernel'));
        $app->setAutoExit(false);

        $input = new StringInput($command);
        $output = new Output\BufferedOutput();

        $error = $app->run($input, $output);

        if($error != 0)
            $msg = "Error: $error";
        else
            $msg = $output->getBuffer();
        return $msg;
    }
}
