<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Output;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayController extends Controller
{

    /**
     * @Route("/pay/check/", name="payment_check_status")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function paymentCheckAction(Request $request)
    {
        $discussionId = $request->get('discussionId');
        $discussion = $this->container->get('doctrine')->getRepository('AppBundle:Discussion')->findOneBy(array('id' => $discussionId));
        $payment = $this->container->get('doctrine')->getRepository('AppBundle:Payment')->findOneBy(array(
            'discussion' => $discussion,
        ));

        if ($payment && $payment->getCompleted()) {
            $status = true;
        } else {
            $status = false;
        }

        return new JsonResponse(array('completed' => $status));
    }

    /**
     * @Route("/pay/success/", name="payment_success")
     */
    public function successUrlAction(Request $request)
    {
        $paymentManager = $this->container->get('app.payment_manager');
        $xml = $request->request->get('xml');
        $sign = $request->request->get('sign');
        $result = $paymentManager->checkXmlResponse($xml, $sign);
        $response = $result->status ? "<script>window.close()</script>" : $result->message ;

        return new Response($response);
    }

    /**
     * @Route("/pay/fail/", name="payment_fail")
     */
    public function failUrlAction(Request $request)
    {
        return new Response('Счет не был оплачен. Пожалуйста, повторите попытку оплаты');
    }

    /**
     * @Route("/pay/result/", defaults={"_format"="xml"}, name="payment_result")
     */
    public function resultUrlAction(Request $request)
    {
        $paymentManager = $this->container->get('app.payment_manager');
        $xml = $request->request->get('xml');
        $sign = $request->request->get('sign');
        $result = $paymentManager->checkXmlResponse($xml, $sign);
        $response = $paymentManager->generateXmlResponse($result->status, $result->message);

        return new Response($response);
    }

    /**
     * @Template()
     * @Route("/pay/generate/", name="payment_generate")
     */
    public function generatePayAction(Request $request)
    {
        $amount = $request->get('amount');
        $discussionId = $request->get('discussionId');

        $xml = $this->get('app.payment_manager')->generateXmlRequest($amount, $discussionId);
        $sign = $this->get('app.payment_manager')->generateSign($xml);

        $result['xml'] = base64_encode($xml);
        $result['sign'] = base64_encode($sign);

        return array('result' => $result);
    }
}
