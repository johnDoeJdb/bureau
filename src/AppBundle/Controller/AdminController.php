<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Employee;
use AppBundle\Entity\HtmlBlock;
use AppBundle\Entity\MenuItem;
use AppBundle\Entity\MetaTags;
use AppBundle\Entity\Order;
use AppBundle\Entity\PaymentSystem;
use AppBundle\Entity\Service;
use AppBundle\Entity\Snippet;
use AppBundle\Entity\User;
use AppBundle\Entity\Vacancy;
use AppBundle\Form\Type\AddressType;
use AppBundle\Form\Type\EmployeeType;
use AppBundle\Form\Type\HtmlBlockType;
use AppBundle\Form\Type\MenuItemType;
use AppBundle\Form\Type\MetaTagsType;
use AppBundle\Form\Type\OrderType;
use AppBundle\Form\Type\PaymentSystemType;
use AppBundle\Form\Type\ServiceType;
use AppBundle\Form\Type\SnippetType;
use AppBundle\Form\Type\SocialButtonsType;
use AppBundle\Form\Type\UserType;
use AppBundle\Form\Type\VacancyType;
use AppBundle\Model\SocialButtons;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends Controller
{
    /**
     * @Template()
     * @Route("/admin/", name="admin_index")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('admin_chats', array('type' => 'new'));
    }

    /**
     * @Template()
     * @Route("/admin/users", name="admin_users")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function usersAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT u FROM AppBundle:User u');
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Template()
     * @Route("/admin/create-user", name="admin_create_user")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->getData();
            $userManager = $this->get('fos_user.user_manager');
            try {
                if ($user->getFile()) {
                    $user->upload($user->getEmail());
                }
                $user->setPlainPassword($user->getPassword());
                $user->setPassword(null);
                $user->setEnabled(true);
                $userManager->updateUser($user);

                return $this->redirectToRoute('admin_users');
            } catch (\Exception $e) {}
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/admin/user/edit/{id}", name="admin_edit_user")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editUserAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if (!$user) {
            return $this->redirectToRoute('admin_users');
        }
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $userManager = $this->get('fos_user.user_manager');
            if ($user->getFile()) {
                $user->upload($user->getEmail());
            }
            $user->setPlainPassword($user->getPassword());
            $user->setPassword(null);
            $userManager->updateUser($user);

            return $this->redirectToRoute('admin_users');
        }

        return array('form' => $form->createView(), 'user' => $user);
    }

    /**
     * @Route("/admin/users/remove/{id}", name="admin_remove_user")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeUserAction(Request $request, $id = 0)
    {
        if ($id) {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array('id' => $id));
            $userManager->deleteUser($user);
        }

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Template()
     * @Route("/admin/chats", name="admin_chats")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function chatsAction(Request $request)
    {
        $type = $request->query->get('type');
        $operatorId = $this->getUser()->getId();
        /** @var $queryBuilder QueryBuilder */
        $query = $this->getDoctrine()->getManager()->getRepository('AppBundle:Discussion')->getDiscussionByTypeQuery($type, $operatorId);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return array('pagination' => $pagination, 'type' => $type);
    }

    /**
     * @Template()
     * @Route("/admin/chat/{id}", name="admin_chat")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function chatAction(Request $request, $id)
    {
        $messages = $this->getDoctrine()->getRepository('AppBundle:Message')->getMessagesBydDiscussion($id);
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($id);
        $payment = $this->getDoctrine()->getRepository('AppBundle:Payment')->findOneBy(array('discussion' => $discussion));
        $paymentCompleted = $payment && $payment->getCompleted() ?  $payment->getCompleted() : false;
        $newMessages = [];
        foreach ($messages as $message) {
            $newMessages[] = array(
                'text' =>  $message->getText(),
                'time' =>  (new \DateTime())->setTimestamp($message->getTimestamp())->format('H:i'),
                'title' => $message->getOperator() ? 'Оператор' : 'Клиент'
            );
        }
        $userId = $this->getUser()->getId();

        return array(
            'messages' => $newMessages,
            'userId' => $userId,
            'discussionId' => $id,
            'user' => $this->getUser(),
            'client' => $discussion->getClient(),
            'paymentCompleted' => $paymentCompleted,
        );
    }

    /**
     * @Template()
     * @Route("/admin/address", name="admin_edit_address")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAddressAction(Request $request)
    {
        $companies = $this->getDoctrine()->getRepository('AppBundle:Company')->findAll();
        if (count($companies)) {
            $company = $companies[0];
        } else {
            $company = new Company();
        }
        $address = $company->getAddress();
        if (!$address) {
            $address = new Address();
        }
        $form = $this->createForm(new AddressType(), $address);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $company->setAddress($data);
            $entityManager->persist($data);
            $entityManager->persist($company);
            $entityManager->flush();
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/admin/employees", name="admin_employees")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function employeesAction(Request $request)
    {
        $employees = $this->getDoctrine()->getRepository('AppBundle:Employee')->findAll();

        return array('employees' => $employees);
    }

    /**
     * @Template()
     * @Route("/admin/edit/employee/{id}", name="admin_edit_employee")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editEmployeeAction(Request $request, $id = 0)
    {
        $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($id);
        if (!$employee) {
            $employee = new Employee();
        }
        $form = $this->createForm(new EmployeeType(), $employee);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $employee = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($employee);
            $entityManager->flush();

            return $this->redirectToRoute('admin_employees');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/employee/{id}", name="admin_remove_employee")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeEmployeeAction(Request $request, $id = 0)
    {
        $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($id);
        if ($employee) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($employee);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_employees');
    }

    /**
     * @Template()
     * @Route("/admin/vacancies", name="admin_vacancies")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function vacanciesAction(Request $request)
    {
        $vacancies = $this->getDoctrine()->getRepository('AppBundle:Vacancy')->findAll();

        return array('vacancies' => $vacancies);
    }

    /**
     * @Route("/admin/remove/vacancy/{id}", name="admin_remove_vacancy")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeVacancyAction(Request $request, $id = 0)
    {
        $vacancy = $this->getDoctrine()->getRepository('AppBundle:Vacancy')->find($id);
        if ($vacancy) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vacancy);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_vacancies');
    }

    /**
     * @Template()
     * @Route("/admin/edit/vacancy/{id}", name="admin_edit_vacancy")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editVacancyAction(Request $request, $id = 0)
    {
        $vacancy = $this->getDoctrine()->getRepository('AppBundle:Vacancy')->find($id);
        if (!$vacancy) {
            $vacancy = new Vacancy();
        }
        $form = $this->createForm(new VacancyType(), $vacancy);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $vacancy = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vacancy);
            $entityManager->flush();

            return $this->redirectToRoute('admin_vacancies');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Template()
     * @Route("/admin/orders", name="admin_orders")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ordersAction(Request $request)
    {
        $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findAll();

        return array('orders' => $orders);
    }

    /**
     * @Template()
     * @Route("/admin/edit/order/{id}", name="admin_edit_order")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editOrderAction(Request $request, $id = 0)
    {
        $order = $this->getDoctrine()->getRepository('AppBundle:Order')->find($id);
        if (!$order) {
            $order = new Order();
        }
        $form = $this->createForm(new OrderType(), $order);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $order = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();

            return $this->redirectToRoute('admin_orders');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/order/{id}", name="admin_remove_order")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeOrderAction(Request $request, $id = 0)
    {
        $order = $this->getDoctrine()->getRepository('AppBundle:Order')->find($id);
        if ($order) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($order);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_orders');
    }

    /**
     * @Template()
     * @Route("/admin/services", name="admin_services")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function servicesAction(Request $request)
    {
        $services = $this->getDoctrine()->getRepository('AppBundle:Service')->findAll();

        return array('services' => $services);
    }

    /**
     * @Template()
     * @Route("/admin/social-buttons", name="admin_social_buttons")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function socialButtonsAction(Request $request)
    {
        $model = new SocialButtons();
        if (!$vkontakteSnippet = $this->getDoctrine()->getRepository('AppBundle:Snippet')->findOneBy(array('name' => 'vkontakte_url'))) {
            $vkontakteSnippet = new Snippet();
        }
        if (!$facebookSnippet = $this->getDoctrine()->getRepository('AppBundle:Snippet')->findOneBy(array('name' => 'facebook_url'))) {
            $facebookSnippet = new Snippet();
        }
        if (!$instagramSnippet = $this->getDoctrine()->getRepository('AppBundle:Snippet')->findOneBy(array('name' => 'instagram_url'))) {
            $instagramSnippet = new Snippet();
        }
        $model->setVkontakte($vkontakteSnippet->getContext());
        $model->setFacebook($facebookSnippet->getContext());
        $model->setInstagram($instagramSnippet->getContext());
        $form = $this->createForm(new SocialButtonsType(), $model);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $vkontakte = $form->get("vkontakte")->getData();
            $facebook= $form->get("facebook")->getData();
            $instagram =  $form->get("instagram")->getData();

            $vkontakteSnippet->setContext($vkontakte);
            $vkontakteSnippet->setName('vkontakte_url');
            $facebookSnippet->setContext($facebook);
            $facebookSnippet->setName('facebook_url');
            $instagramSnippet->setContext($instagram);
            $instagramSnippet->setName('instagram_url');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vkontakteSnippet);
            $entityManager->persist($facebookSnippet);
            $entityManager->persist($instagramSnippet);
            $entityManager->flush();
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/admin/edit/service/{id}", name="admin_edit_service")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editServiceAction(Request $request, $id = 0)
    {
        $service = $this->getDoctrine()->getRepository('AppBundle:Service')->find($id);
        if (!$service) {
            $service = new Service();
        }
        $form = $this->createForm(new ServiceType(), $service);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $service = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($service);
            $entityManager->flush();

            return $this->redirectToRoute('admin_services');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/service/{id}", name="admin_remove_service")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeServiceAction(Request $request, $id = 0)
    {
        $service = $this->getDoctrine()->getRepository('AppBundle:Service')->find($id);
        if ($service) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($service);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_services');
    }


    /**
     * @Template()
     * @Route("/admin/snippets", name="admin_snippets")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function snippetsAction(Request $request)
    {
        $snippets = $this->getDoctrine()->getRepository('AppBundle:Snippet')->findAll();

        return array('snippets' => $snippets);
    }


    /**
     * @Template()
     * @Route("/admin/edit/snippet/{id}", name="admin_edit_snippet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editSnippetAction(Request $request, $id = 0)
    {
        $snippet = $this->getDoctrine()->getRepository('AppBundle:Snippet')->find($id);
        if (!$snippet) {
            $snippet = new Snippet();
        }
        $form = $this->createForm(new SnippetType(), $snippet);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $snippet = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($snippet);
            $entityManager->flush();

            return $this->redirectToRoute('admin_snippets');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/snippet/{id}", name="admin_remove_snippet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeSnippetAction(Request $request, $id = 0)
    {
        $snippet = $this->getDoctrine()->getRepository('AppBundle:Snippet')->find($id);
        if ($snippet) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($snippet);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_snippets');
    }

    /**
     * @Template()
     * @Route("/admin/menu-items", name="admin_menu_items")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function menuItemsAction(Request $request)
    {
        $menuItems = $this->getDoctrine()->getRepository('AppBundle:MenuItem')->findAll();

        return array('menuItems' => $menuItems);
    }

    /**
     * @Template()
     * @Route("/admin/edit/menu-item/{id}", name="admin_edit_menu_item")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editMenuItemAction(Request $request, $id = 0)
    {
        $menuItem = $this->getDoctrine()->getRepository('AppBundle:MenuItem')->find($id);
        if (!$menuItem) {
            $menuItem = new MenuItem();
        }
        $form = $this->createForm(new MenuItemType(), $menuItem);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $menuItem = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menuItem);
            $entityManager->flush();

            return $this->redirectToRoute('admin_menu_items');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/menu-item/{id}", name="admin_remove_menu_item")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeMenuItemAction(Request $request, $id = 0)
    {
        $menuItem = $this->getDoctrine()->getRepository('AppBundle:MenuItem')->find($id);
        if ($menuItem) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($menuItem);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_menu_items');
    }

    /**
     * @Template()
     * @Route("/admin/vacancy/responses", name="admin_vacancy_responses")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function vacancyResponsesAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT v FROM AppBundle:SubmitVacancy v ORDER BY v.id DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Template()
     * @Route("/admin/business/responses", name="admin_business_responses")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function businessResponsesAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT v FROM AppBundle:SubmitBusiness v ORDER BY v.id DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Template()
     * @Route("/admin/metatags", name="admin_metatags")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function metaTagsAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT v FROM AppBundle:MetaTags v ORDER BY v.id DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Template()
     * @Route("/admin/metatags/edit/{id}", name="admin_edit_metatag")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editMetaTagAction(Request $request, $id = 0)
    {
        $metaTag = $this->getDoctrine()->getRepository('AppBundle:MetaTags')->find($id);
        if (!$metaTag) {
            $metaTag = new MetaTags();
        }
        $form = $this->createForm(new MetaTagsType(), $metaTag);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $metaTag = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($metaTag);
            $entityManager->flush();

            return $this->redirectToRoute('admin_metatags');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/metatags/remove/{id}", name="admin_remove_metatag")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeMetaTagAction(Request $request, $id)
    {
        $metaTag = $this->getDoctrine()->getRepository('AppBundle:MetaTags')->find($id);
        if ($metaTag) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($metaTag);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_metatags');
    }

    /**
     * @Template()
     * @Route("/admin/mobile-application-requests", name="admin_mobile_application_requests")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function mobileApplicationRequestsAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT v FROM AppBundle:MobileApplicationRequest v ORDER BY v.id DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Route("/admin/mobile-application-request/remove/{id}", name="admin_remove_mobile_application_request")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeMobileApplicationRequestAction(Request $request, $id)
    {
        $mobileApplicationRequest = $this->getDoctrine()->getRepository('AppBundle:MobileApplicationRequest')->find($id);
        if ($mobileApplicationRequest) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mobileApplicationRequest);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_mobile_application_requests');
    }

    /**
     * @Route("/admin/send-email", name="admin_send_email")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function sendClientEmailAction(Request $request)
    {
        $email = $request->request->get('email');
        $subject = $request->request->get('subject');
        $message = $request->request->get('message');
        if ($email || $subject || $message) {
            $this->get('app.mailer')->sendMail($email, $subject, $message);
        }

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @Template()
     * @Route("/admin/html-blocks", name="admin_html_blocks")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function htmlBlocksAction(Request $request)
    {
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT v FROM AppBundle:HtmlBlock v ORDER BY v.id DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('pagination' => $pagination);
    }

    /**
     * @Template()
     * @Route("/admin/edit/html-block/{id}", name="admin_edit_html_block")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editHtmlBlockAction(Request $request, $id = 0)
    {
        $htmlBlock = $this->getDoctrine()->getRepository('AppBundle:HtmlBlock')->find($id);
        if (!$htmlBlock) {
            $htmlBlock = new HtmlBlock();
        }
        $form = $this->createForm(new HtmlBlockType(), $htmlBlock);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $menuItem = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menuItem);
            $entityManager->flush();

            return $this->redirectToRoute('admin_html_blocks');
        }

        return array('form' => $form->createView(), 'id' => $id);
    }

    /**
     * @Route("/admin/remove/html-block/{id}", name="admin_remove_html_block")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeHtmlBlockAction(Request $request, $id)
    {
        $metaTag = $this->getDoctrine()->getRepository('AppBundle:HtmlBlock')->find($id);
        if ($metaTag) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($metaTag);
            $entityManager->flush();

        }

        return $this->redirectToRoute('admin_html_blocks');
    }

    /**
     * @Template()
     * @Route("/admin/payment-system", name="admin_payment_system")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function paymentSystemAction(Request $request)
    {
        $paymentSystem = $this->getDoctrine()->getManager()->getRepository('AppBundle:PaymentSystem')->findOneBy(array());
        if (!$paymentSystem) {
            $paymentSystem = new PaymentSystem();
        }
        $form = $this->createForm(new PaymentSystemType(), $paymentSystem);
        $form->handleRequest($request);
        if ($form->isValid() && $request->getMethod() === 'POST') {
            $menuItem = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menuItem);
            $entityManager->flush();
        }

        return array('form' => $form->createView());
    }
}
