<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\Discussion;
use AppBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class ChatController extends Controller
{
    /**
     * @Route("/messages/{discussionId}", name="homepage")
     */
    public function getMessagesForDiscussionAction($discussionId)
    {
        $messages = $this->getDoctrine()->getRepository('AppBundle:Message')->getMessagesBydDiscussion($discussionId);

        return new JsonResponse($messages);
    }

    /**
     * @Cache(expires="tomorrow", public=true)
     * @Route("/server/ip", name="get_server_ip")
     */
    public function getServerIpAction()
    {
        $ip = $_SERVER['SERVER_ADDR'];

        return new JsonResponse(['ip' => $ip]);
    }

    /**
     * @Route("/messages/create-discussion", name="create_discussion")
     */
    public function createDiscussionAction(Request $request)
    {
        $discussion = new Discussion();
        $client = new Client();
        $discussion->setTimestamp((new \DateTime())->getTimestamp());
        $discussion->setClient($client);
        $this->getDoctrine()->getManager()->persist($discussion);
        $this->getDoctrine()->getManager()->persist($client);
        $this->getDoctrine()->getManager()->flush();

        $data = [
            'discussionId' => $discussion->getId(),
            'userId' => $client->getId(),
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/messages/stop-discussion/{discussionId}", name="stop_discussion")
     */
    public function stopDiscussionAction(Request $request, $discussionId)
    {
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($discussionId);
        $discussion->setIsActiveClient(false);

        $this->getDoctrine()->getManager()->persist($discussion);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('status' => 'ok'));
    }

    /**
     * @Route("/messages/send/{discussionId}", name="client_send_message")
     */
    public function sendClientMessageAction(Request $request, $discussionId)
    {
        $text = $request->request->get('message');
        $clientId = $request->request->get('clientId');
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($discussionId);
        $client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($clientId);
        $message = new Message();
        $message->setTimestamp((new \DateTime())->getTimestamp());
        $message->setClient($client);
        $message->setText($text);
        $message->setDiscussion($discussion);
        $this->getDoctrine()->getManager()->persist($message);
        $this->getDoctrine()->getManager()->flush($message);

        return new JsonResponse('"status": "ok"');
    }

    /**
     * @Route("/client-details/send/{id}", name="client_details_send")
     */
    public function sendClientDetailsAction(Request $request, $id)
    {
        $email = $request->request->get('email');
        $phone = $request->request->get('phone');
        $fullName = $request->request->get('fullName');
        $client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($id);
        if ($client) {
            $client->setEmail($email);
            $client->setPhone($phone);
            $client->setFullName($fullName);
            $this->getDoctrine()->getManager()->persist($client);
            $this->getDoctrine()->getManager()->flush($client);
        }

        return new JsonResponse('"status": "ok"');
    }

    /**
     * @Route("/admin/messages/{discussionId}", name="admin_send_message")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function sendOperatorMessageAction(Request $request, $discussionId)
    {
        $text = $request->request->get('message');
        $userId = $request->request->get('userId');
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($userId);
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($discussionId);
        $message = new Message();
        $message->setOperator($user);
        $message->setText($text);
        $message->setDiscussion($discussion);
        $message->setTimestamp((new \DateTime())->getTimestamp());
        $this->getDoctrine()->getManager()->persist($message);
        $this->getDoctrine()->getManager()->flush($message);

        $response = [
            'text' =>   $message->getText(),
            'time' =>   (new \DateTime())->setTimestamp($message->getTimestamp()),
            'title' =>   'Оператор',
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/start-discussion/{discussionId}", name="admin_start_discussion")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function startDiscussionOperatorAction(Request $request, $discussionId)
    {
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($discussionId);
        $user = $this->getUser();
        $discussion->setOperator($user);
        $discussion->setIsActiveOperator(true);
        $this->getDoctrine()->getManager()->persist($discussion);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('status' => 'ok'));
    }

    /**
     * @Route("/admin/stop-discussion/{discussionId}", name="admin_stop_discussion")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function stopDiscussionOperatorAction(Request $request, $discussionId)
    {
        $discussion = $this->getDoctrine()->getRepository('AppBundle:Discussion')->find($discussionId);
        $discussion->setIsActiveOperator(false);
        $this->getDoctrine()->getManager()->persist($discussion);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(array('status' => 'ok'));
    }

    /**
     * @Route("/admin/discussions/get-next/{timestamp}", name="admin_get_next_discussions")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function getNextDiscussionsAction(Request $request, $timestamp)
    {
        $discussions = $this->getDoctrine()->getRepository('AppBundle:Discussion')->getNextDiscussions($timestamp);

        return new JsonResponse(array('items' => $discussions));
    }

    /**
     * @Route("/admin/get-current-time", name="admin_get_current_time")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OPERATOR')")
     */
    public function getCurrentTimeAction()
    {
        $currentTimestamp = (new \DateTime())->getTimestamp();

        return new JsonResponse(array('timestamp' => $currentTimestamp));
    }
}
