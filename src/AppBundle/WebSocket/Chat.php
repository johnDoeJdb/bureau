<?php
namespace AppBundle\WebSocket;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Constraints\DateTime;

class Chat implements WampServerInterface {
    protected $clients;
    protected $operators;
    protected $doctrine;
    protected $container;

    public function __construct(Registry $doctrine, Container $container) {
        $this->clients = array();
        $this->operators = array();
        $this->doctrine = $doctrine;
        $this->container = $container;
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        $event = json_decode($event);
        $event->time = (new \DateTime())->format('H:i');
        $topic->broadcast(json_encode($event));
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        $conn->callError($id, $topic, 'RPC not supported on this demo');
    }
    // No need to anything, since WampServer adds and removes subscribers to Topics automatically
    public function onSubscribe(ConnectionInterface $conn, $topic) { }
    public function onUnSubscribe(ConnectionInterface $conn, $topic) { }
    public function onOpen(ConnectionInterface $conn) {
        $querystring = $conn->WebSocket->request->getQuery();
        try {
            parse_str($querystring, $parameters);
            $id = $parameters['discussionId'];
            $type = $parameters['type'];
        } catch (\Exception $e) {
//            $conn->close();
            return;
        }
        $discussion = $this->doctrine->getRepository('AppBundle:Discussion')->find($id);
        if (!$discussion) {
            $conn->close();
            return;
        }
        if ($type == 'client') {
            $this->clients[$id] = $conn;
            $discussion->setIsActiveClient(true);
        } else {
            $this->operators[$id] = $conn;
            $discussion->setIsActiveOperator(true);
        }
        $this->doctrine->getManager()->persist($discussion);
        $this->doctrine->getManager()->flush();
    }
    public function onMessage(ConnectionInterface $from, $msg) { }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        self::closeDiscussion($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    private function closeDiscussion($connection) {
        foreach ($this->clients as $id => $client) {
            if ($client == $connection) {
                $discussion = $this->doctrine->getRepository('AppBundle:Discussion')->find($id);
                if ($discussion) {
                    $discussion->setIsActiveClient(false);
                    $this->doctrine->getManager()->persist($discussion);
                    $this->doctrine->getManager()->flush();
                    unset($this->clients[$id]);
                }
            }
        }
        foreach ($this->operators as $id => $operator) {
            if ($operator == $connection) {
                $discussion = $this->doctrine->getRepository('AppBundle:Discussion')->find($id);
                if ($discussion) {
                    $discussion->setIsActiveOperator(false);
                    $this->doctrine->getManager()->persist($discussion);
                    $this->doctrine->getManager()->flush();
                    unset($this->operators[$id]);
                }
            }
        }
    }
}