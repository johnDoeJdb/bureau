var Bureau = angular.module('bureau', ['ngMask', 'hj.stickyContainer', 'ngAnimate', 'ngSanitize']);

Bureau.config(['$interpolateProvider',
    function ($interpolateProvider) {
        // Setup default markup for templates to avoid conflicts with other templates engines
        $interpolateProvider.startSymbol('ng{');
        $interpolateProvider.endSymbol('}ng');
    }
]);

Bureau.run(['$rootScope', '$http', '$timeout', function ($rootScope, $http, $timeout) {
    $http.post('/server/ip', {})
        .success(function(data, status, headers, config) {
            $rootScope.serverIp = data.ip;
        });
    $rootScope.slideOffset = 0;
    $rootScope.client = {};
    $rootScope.successPopup = {};
    $rootScope.popup = {};
    $rootScope.itemSlide = {};
    $rootScope.chatMessage = '';
    $rootScope.hidden = true;
    $rootScope.popupVisibility = false;
    $rootScope.successPopup.visibility = false;
    $rootScope.popup.email = '';
    $rootScope.popupMenu = false;
    $rootScope.popupMenuWidth = 0;
    $rootScope.operatorResponded = false;
    $rootScope.firstMessage = false;
    $rootScope.clientSentDetails = false;
    $rootScope.operatorNotified = false;
    $rootScope.rightArrow = false;

    $rootScope.toParams = function(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };
}]);

Bureau.controller('ChatController', ['$scope', '$http', '$timeout', '$q', '$rootScope', function($scope, $http, $timeout, $q, $rootScope) {
    $scope.startTryingConnect = false;
    $scope.waitingForm = false;
    $scope.messages = [];

    $scope.startConnection = function() {
        if (!$scope.startedConnection) {
            $scope.startedConnection = true;
        }
        $scope.startTryingConnect = true;
        setTimeout(function() {$scope.startTryingConnect = false;}, 10000);
        ab.connect(
            'ws://'+$scope.serverIp+':81?discussionId=' + $scope.discussionId + '&type=client',
            connectSuccess,
            connectFailure,
            {
                'maxRetries': 0,
                'retryDelay': 0
            }
        );

        function onEvent(topic, event) {
            var message = JSON.parse(event);
            if (message.type != 'request') {
                $scope.waitingForm = false;
                if (!$rootScope.operatorResponded) {
                    $rootScope.$apply(function() {
                        $rootScope.operatorResponded = true;
                    })
                }
            }
            $scope.$apply(function() {
                $scope.messages.push(message);
            });
            $(".nano").nanoScroller({ destroy: true });
            setTimeout(function() {
                $(".nano").nanoScroller();
                $('.nano-content').scrollTop(999999999)
            }, 50);
            $rootScope.soundChatMessage.play();
        }
        function connectSuccess(session) {
            $scope.connection = session;
            if (!$scope.established) {
                $scope.connection.subscribe('chat#'+$scope.discussionId, onEvent);
                $scope.subscribed = true;
                if (!$rootScope.operatorNotified) {
                    var element = {};
                    element.text = 'Новый чат №'+$scope.discussionId;
                    element.discussionId = $scope.discussionId;
                    $scope.connection.publish('operatorNotification', JSON.stringify(element));
                    $rootScope.soundStartChat.play();
                    $rootScope.operatorNotified = true;
                }
            }
            $scope.established = true;
            $scope.startTryingConnect = false;
        }
        function connectFailure(failure) {
            console.log(failure);
            if (!$scope.established) {
                try {
                    $scope.connection.unsubscribe('chat#'+$scope.discussionId);
                    $scope.subscribed = false;
                } catch (e) {}
            }
            $scope.established = false;
            $scope.startTryingConnect = false;

            setTimeout($scope.startConnection, 1000);
        }
    };

    $scope.startDiscussion = function() {
        var deferred = $q.defer();

        $scope.isDiscussionStarted = true;
        $http.post('/messages/create-discussion',({}))
            .success(function(data, status, headers, config) {
                $scope.discussionId = data.discussionId;
                $scope.userId = data.userId;
                deferred.resolve('Discussion created');
            })
            .error(function(data, status, headers, config) {
                deferred.reject('Discussion did not created');
            });

        return deferred.promise
    };

    $scope.sendMessageToServer = function(message) {
        var timestamp = Date.now() / 1000 | 0;
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $http.post('/messages/send/' + $scope.discussionId, $scope.toParams({'message': message, 'clientId': $scope.userId, 'timestamp': timestamp}));
    };

    $rootScope.prepareSuccessPopup = function(event) {
        var element = angular.element(event.currentTarget);
        var successTitle = element.attr('ng-success-title');
        var successText = element.attr('ng-success-text');
        if (successTitle && successText) {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.successPopup.title = successTitle;
                    $rootScope.successPopup.text = successText;
                });
            });
        }
    };

    $rootScope.sendClientDetails = function(event, withoutMessage) {
        var element = angular.element(event.target);
        var successTitle = element.attr('ng-success-title');
        var successText = element.attr('ng-success-text');
        var details = {'phone': $scope.client.phone, 'email': $scope.client.email, 'fullName': $scope.client.fullName, 'type': 'details'};
        if (!withoutMessage) {
            $rootScope.chatMessage = $scope.client.message;
        }
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $http.post('/client-details/send/' + $scope.userId, $scope.toParams(details))
            .success(function(data, status, headers, config) {
                $scope.sendMessage();
                $scope.connection.publish('service_chat#'+$scope.discussionId, JSON.stringify(details));
                $scope.client.phone = '';
                $scope.client.email = '';
                $scope.client.fullName = '';
                $scope.client.message = '';
                $timeout(function() {
                    if (!$rootScope.operatorResponded) {
                        $rootScope.clientSentDetails = true;
                        $rootScope.hidden = true;
                        $scope.$apply(function() {
                            $scope.waitingForm = false;
                        });
                    } else {
                        $scope.$apply(function() {
                            $scope.waitingForm = false;
                            $scope.contactForm = false;
                        });
                    }
                    if (successTitle && successText) {
                        $rootScope.$apply(function() {
                            $rootScope.successPopup.visibility = true;
                            $rootScope.successPopup.title = successTitle;
                            $rootScope.successPopup.text = successText;
                        });
                    }
                });
            });
    };

    $rootScope.sendMessage = function() {
        if ($rootScope.hidden) {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.hidden = false;
                });
            });
        }
        if (!$scope.isDiscussionStarted) {
            $scope.startDiscussion().then($scope.startConnection);
        }
        var message = $rootScope.chatMessage;
        $rootScope.chatMessage = '';
        if (message) {
            function wait() {
                if ($scope.established){
                    $scope.connection.publish('chat#'+$scope.discussionId, JSON.stringify(element));
                    $scope.sendMessageToServer(message);
                }
                else {
                    setTimeout(function() {
                        wait();
                    }, 1000);
                }
            }
            var element = {'text': message, 'type': 'request', 'avatar': 'https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg'};
            wait();
        }
    };

    $scope.sendAjaxForm = function(event) {
        var element = angular.element(event.target);
        var form = element.parent();
        var formName = form.attr('name');
        var formModel = form.attr('form-model');
        $( form )
            .submit( function( e ) {
                e.preventDefault();
                $.ajax({
                    url: $scope.ajaxFormUrl,
                    type: 'POST',
                    data: new FormData( this ),
                    processData: false,
                    contentType: false
                });
                $scope.$apply(function() {
                    $scope[formModel] = {};
                    $scope[formName].$setPristine(true);
                    $scope[formName].$setUntouched(true);
                });
                $rootScope.$apply(function() {
                    $rootScope.successPopup.visibility = true;
                });
                try {
                    document.querySelectorAll(".js-file-input")[0].value = '';
                } catch (e) {};
                return false;
            });
        return false;
    };

    var offset = 269;
    $rootScope.nextSlide = function() {
        var slides = 1;
        angular.forEach($scope.itemSlide, function(value, key) {
            if (key > slides) {
                slides = key;
            }
        });
        if (Math.abs($rootScope.slideOffset) < offset * ($rootScope.totalSlides-$rootScope.sliderVisibleElements) ) {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.slideOffset -= offset;
                })
            });
        } else {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.slideOffset = 0;
                })
            });
        }
    };

    $rootScope.previousSlide = function() {
        var slides = 1;
        angular.forEach($scope.itemSlide, function(value, key) {
            if (key > slides) {
                slides = key;
            }
        });
        if ($rootScope.slideOffset < 0) {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.slideOffset += offset;
                })
            });
        } else {
            $timeout(function() {
                $rootScope.$apply(function() {
                    $rootScope.slideOffset = - offset * ($rootScope.totalSlides-$rootScope.sliderVisibleElements);
                })
            });
        }
    };

    $rootScope.resetSlider = function() {
        $timeout(function() {
            $rootScope.$apply(function() {
                $rootScope.slideOffset = 0;
            });
        });
    };

    $rootScope.$watch('hidden', function() {
        if (!$rootScope.hidden && !$rootScope.clientSentDetails) {
            $timeout(function() {
                if (!$rootScope.operatorResponded) {
                    $scope.$apply(function() {
                        $scope.waitingForm = true;
                        $('.nano-content').scrollTop(0)
                    });
                }
            }, $rootScope.waitingFormTime);
        }
    });
}]);

Bureau.directive('ngBureauChat', ['$templateRequest', '$compile', '$rootScope', function($templateRequest, $compile, $rootScope) {
    function link(scope, element, attrs, ctrl) {
        scope.$watch('messages.length', function() {
            showMessages();
        });
        function showMessages() {
            angular.forEach(scope.messages, function(message, key) {
                if (message.handeled !== true) {
                    var previousType,
                        template;
                    if (key != 0) {
                        previousType = scope.messages[key-1].type;
                    }
                    if (message.text) {
                        message.text = message.text.replace(/((?:https|http|ftp)\:\/\/\S+)/g, '<a href="$1">$1</a> ');
                    }
                    if (message.type === 'request' && previousType !== 'request') {
                        $templateRequest("b-message__request.html").then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'request' && previousType === 'request') {
                        $templateRequest('b-message__request-without-avatar.html').then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'response' && previousType !== 'response') {
                        $templateRequest('b-message__response.html').then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            isolatedScope.avatar = message.avatar;
                            isolatedScope.username = message.title;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'response' && previousType === 'response') {
                        $templateRequest('b-message__response-without-avatar.html').then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'order') {
                        $templateRequest('b-message__order.html').then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            isolatedScope.sum = message.sum;
                            isolatedScope.avatar = message.avatar;
                            isolatedScope.username = message.title;
                            isolatedScope.discussionId = scope.discussionId;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'contact-form') {
                        $templateRequest('b-contact-form.html').then(function(html){
                            var content = $compile(html)(scope);
                            element.append(content);
                        });
                    }
                    if (message.type === 'payed') {
                        $templateRequest('b-payed.html').then(function(html){
                            var isolatedScope = scope.$new(true);
                            isolatedScope.message = message.text;
                            isolatedScope.time = message.time;
                            var content = $compile(html)(isolatedScope);
                            element.append(content);
                        });
                    }
                    message.handeled = true;
                }
            });
        }
        showMessages();
    }

    return {
        link: link,
    };
}]);

Bureau.directive('ngChatInput', [function() {
    function link(scope, element, attrs) {
        element.on("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.sendMessage();
            }
        });
    }

    return {
        link: link
    };
}]);

Bureau.directive('ngQuickMessage', ['$rootScope', function($rootScope) {
    function link(scope, element, attributes, ctrl) {
        element.on('click', function() {
            var message = attributes.ngMessage;
            updateMessage(message);
        });

        function updateMessage(message) {
            scope.$apply(function() {
                scope.chatMessage = message + " ";
            });
            $('html, body').animate({
                    scrollTop: $('.e-input-text__top-content').offset().top - 50 },
                'slow', function() {
                    $('.e-input-text__top-content').focus();
                });
        }
    }

    return {
        link: link,
    };
}]);

Bureau.directive('ngRussianPhoneFormat', [function() {
    function link(scope, element, attributes) {
        scope.phone = '+7';
    }

    return {
        link: link,
    };
}]);

Bureau.directive('ngNotEmpty', [function() {
    function link(scope, element, attributes, ctrl) {
        var modelName = ctrl.$name;
        scope.$watch(modelName, function() {
            doValidation();
        });

        function doValidation() {
            if (!!$(element).val()) {
                ctrl.$setValidity('filled', true);
            } else {
                ctrl.$setValidity('filled', false);
            }
        }

        setTimeout(function() {
            ctrl.$setValidity('filled', true);
        }, 500);
    }

    return {
        require: 'ngModel',
        link: link,
    };
}]);


Bureau.directive('ngChatButton', ['$http', '$timeout', function($http, $timeout) {
    function link(scope, element, attrs) {
        var opennedValue = attrs.opennedValue;
        var closedValue = attrs.closedValue;
        scope.$watch('hidden', function() {
            var width = $(window).width();
            if (width > 640) {
                scope.$watch('hidden', function() {
                    if (scope.hidden) {
                        element.attr('value', closedValue);
                    } else {
                        window.scrollTo(0, 0);
                        element.attr('value', opennedValue);
                    }
                });
            }
        });
        enquire.register("screen and (max-width: 640px)", {
            match : function() {
                element.val('');
            },
            unmatch : function() {
                if (scope.hidden) {
                    element.val('Получить');
                } else {
                    element.val('Отправить');
                }
            }
        });
    }

    return {
        link: link
    };
}]);

Bureau.directive('ngChatContent', ['$http', '$timeout', function($http, $timeout) {
    function link(scope, element, attrs) {
        scope.$watch('hidden', function() {
            if (scope.hidden) {
                element.css('display', 'none');
            } else {
                element.css('display', 'block');
            }
        });
    }

    return {
        link: link
    };
}]);

Bureau.directive('ngPopupSendRequest', ['$http', '$timeout', '$rootScope', function($http, $timeout, $rootScope) {
    function link(scope, element, attributes, ctrl) {
        element.on('click', function() {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $http.post('/request-mobile-application', scope.toParams({ 'email': scope.popup.email, 'platform': scope.mobileApplication }))
                .success(function(data, status, headers, config) {
                    scope.popupVisibility = false;
                    scope.popup.email = '';
                    scope.mobileApplication = '';
                    $timeout(function() {
                        $rootScope.$apply(function() {
                            $rootScope.successPopup.visibility = true;
                        });
                    });
                });
        });
    }

    return {
        link: link,
    };
}]);

Bureau.directive('ngSendVacancyForm', ['$http', '$timeout', '$rootScope', function($http, $timeout, $rootScope) {
    function link(scope, element, attributes, ctrl) {
        element.on('click', function() {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $http.post('/vacancy/submit', scope.toParams({ 'email': scope.popup.email }))
                .success(function(data, status, headers, config) {
                    scope.popupVisibility = false;
                    scope.popup.email = '';
                    $rootScope.apply(function() {
                        $rootScope.successPopup.visibility = true;
                    });
                });
        });
    }

    return {
        link: link,
    };
}]);

Bureau.directive('ngPopupMenu', ['$templateRequest', '$compile', '$rootScope', '$document', '$timeout', function($templateRequest, $compile, $rootScope, $document, $timeout) {
    function link(scope, element, attributes, ctrl) {
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
        var cssClass = attributes.menuContainerCss;
        $templateRequest('b-popup-menu.html').then(function(html) {
            var base64 = attributes.ngMenu;
            var decodedString = Base64.decode(base64);
            var menu = JSON.parse(decodedString);
            var isolatedScope = scope.$new(true);
            isolatedScope.menu = menu;
            var content = $compile(html)(isolatedScope);
            content.insertAfter(element);
        });

        element.on('click', function(event) {
            $rootScope.$apply(function() {
                var height = $("."+cssClass).css('visibility', 'hidden');
                $rootScope.popupMenu = true;
            });
            $timeout(function() {
                var height = $("."+cssClass)[0].clientHeight;
                $rootScope.popupMenuWidth = Math.abs(height)+10;
                $("."+cssClass).css('visibility', 'visible');
            }, 30);
            event.stopPropagation();
        });

        $document.on('click', function() {
            $rootScope.$apply(function() {
                $rootScope.popupMenu = false;
            });
        });
    }

    return {
        link: link,
    };
}]);

Bureau.directive('validFile',function(){
    return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
            el.bind('change',function(){
                scope.$apply(function(){
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});

Bureau.directive('ngSliderContainer',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.sliderActive = attributes.ngSliderContainer;
            $rootScope.sliderVisibleElements = 4;
            $rootScope.previousVisiableElementsNumber = 4;
            var autoslideTimeout = parseInt(attributes.ngSliderAutoslideTimeout) ? parseInt(attributes.ngSliderAutoslideTimeout) : 5000;

            function resizeContainer() {
                var width = $(window).width();
                var numberVisibleElements = Math.round(width / 265 - 1);
                if (numberVisibleElements > 4) {
                    numberVisibleElements = 4;
                } else if (numberVisibleElements <= 1) {
                    numberVisibleElements = 1;
                }
                $rootScope.sliderVisibleElements = numberVisibleElements;
                if (Math.abs($rootScope.previousVisiableElementsNumber - $rootScope.sliderVisibleElements) > 0) {
                    $rootScope.resetSlider();
                    $rootScope.previousVisiableElementsNumber = $rootScope.sliderVisibleElements;
                }
                var sliderWidth = numberVisibleElements*265+50;
                $(element).css('width', sliderWidth + "px");
            }

            if ($rootScope.sliderActive > 0) {
                setInterval(function() {
                    $rootScope.nextSlide();
                }, autoslideTimeout);

                $(window).on('resize', function() {
                    resizeContainer();
                });

                resizeContainer();
            }
        }
    }
});

Bureau.directive('ngWaitingForm',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.waitingFormTime = parseInt(attributes.ngWaitingForm) ? parseInt(attributes.ngWaitingForm) : 15000;
        }
    }
});

Bureau.directive('ngSoundStartChat',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.soundStartChat = element[0];
        }
    }
});

Bureau.directive('ngSoundMessageChat',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.soundChatMessage = element[0];
        }
    }
});

Bureau.directive('ngArrowElement',function($rootScope, $timeout){
    return {
        link:function(scope, element, attributes, ctrl) {
            var width = $(window).width();
            if(width > 640) {
                $timeout(function() {
                    $rootScope.$apply(function(){
                        $rootScope.rightArrow = false;
                    });
                })
            } else {
                $timeout(function() {
                    $rootScope.$apply(function(){
                        $rootScope.rightArrow = true;
                    });
                })
            }
            enquire.register("screen and (max-width: 640px)", {
                match : function() {
                    $timeout(function() {
                        $rootScope.$apply(function(){
                            $rootScope.rightArrow = true;
                        });
                    })
                },
                unmatch : function() {
                    $timeout(function() {
                        $rootScope.$apply(function(){
                            $rootScope.rightArrow = false;
                        });
                    })
                }
            });
        }
    }
});