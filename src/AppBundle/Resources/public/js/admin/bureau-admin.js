var BureauAdmin = angular.module('bureauAdmin', ['ui-notification']);

BureauAdmin.config(['$interpolateProvider', '$httpProvider', 'NotificationProvider',
    function ($interpolateProvider, $httpProvider, NotificationProvider) {
        // Setup default markup for templates to avoid conflicts with other templates engines
        $interpolateProvider.startSymbol('ng{');
        $interpolateProvider.endSymbol('}ng');

        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
    }
]);

BureauAdmin.run(['$rootScope', '$http', '$timeout', function ($rootScope, $http, $timeout) {
    $rootScope.toParams = function (obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };
    $rootScope.avatar = '';
    $rootScope.username = '';
    $rootScope.queue = [];
    $rootScope.startTimestamp = undefined;
    $rootScope.client = {};
    $rootScope.client.email = '';
    $rootScope.client.phone = '';
    $rootScope.client.fullName = '';
    $rootScope.Base64 = {_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

}]);

BureauAdmin.controller('MainController', ['$scope', '$timeout', '$http', '$rootScope', '$location', 'Notification', function($scope, $timeout, $http, $rootScope, $location, Notification) {
    var regex = /\/admin\/chat\/\d+$/;
    if (regex.test(window.location.pathname)) {
        $scope.isChatPage = true;
    }

    $http.post('/server/ip', {})
        .success(function(data, status, headers, config) {
            $rootScope.serverIp = data.ip;
        });

    $scope.onEvent = function(topic, event) {
        var message = JSON.parse(event);
        if (message.type != 'request') {
            message.title = 'Оператор';
        } else {
            message.title = 'Клиент';
        }
        $scope.$apply(function() {
            $scope.messages.push(
                {'text': message.text, 'time': message.time, 'title': message.title, 'type': 'response'}
            );
            Notification(message.text);
            $rootScope.soundChatMessage.play();
        });
    };
    $scope.onServiceEvent = function(topic, event) {
        var message = JSON.parse(event);
        if (message.type == 'details') {
            $scope.$apply(function() {
                $scope.client.email = message.email;
                $scope.client.phone = message.phone;
                $scope.client.fullName = message.fullName;
            });
            Notification.success(message.text);
        }
        if (message.type == 'payment') {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $http.post('/pay/check/', $scope.toParams({'discussionId': $scope.discussionId }))
                .success(function(data, status, headers, config) {
                    console.log(data);
                    $timeout(function() {
                        $scope.$apply(function() {
                            $scope.client.paymentStatus = data.completed ? 'Оплачен' : 'Не оплачен';
                        });
                    });
                });
        }
    };
    $scope.onNewChatEvent = function(topic, event) {
        var message = JSON.parse(event);
        $rootScope.soundStartChat.play();
        Notification.success(message.text);
        if (($.inArray(message.discussionId, $rootScope.queue)) !== -1) {
            $rootScope.queue.append(message.discussionId);
        }
    };

    $rootScope.subscribeToOperatorNotification = function() {
        function connect() {
            if (!$rootScope.establishedConnectionToNotification && $rootScope.serverIp) {
                setTimeout(function() {$scope.startTryingConnectToNotifications = false;}, 10000);
                function connectSuccess(session) {
                    $scope.connectionOperatorNotification = session;
                    if (!$scope.establishedConnectionToNotification) {
                        $scope.connectionOperatorNotification.subscribe('operatorNotification', $scope.onNewChatEvent);
                    }
                    $scope.establishedConnectionToNotification = true;
                    $scope.startTryingConnectToNotifications = false;
                    $rootScope.showMissedDiscussions();
                }
                function connectFailure(failure) {
                    console.log(failure);
                    if (!$scope.establishedConnectionToNotification) {
                        try {
                            $scope.connectionOperatorNotification.unsubscribe('operatorNotification', $scope.onNewChatEvent);
                        } catch (e) {}
                        try {
                            $scope.connectionOperatorNotification = undefined;
                        } catch (e) {}
                    }
                    $scope.establishedConnectionToNotification = false;
                    $scope.startTryingConnectToNotifications = false;
                    $scope.stopNotification();
                    setTimeout($rootScope.subscribeToOperatorNotification, 2000);
                }
                ab.connect(
                    'ws://'+$rootScope.serverIp+':81',
                    connectSuccess,
                    connectFailure,
                    {
                        'maxRetries': 0,
                        'retryDelay': 0
                    }
                );
            }
            else {
                setTimeout($rootScope.subscribeToOperatorNotification, 1000);
            }
        }
        connect();
    };

    $rootScope.showMissedDiscussions = function() {
        if (!$rootScope.startTimestamp) {
            $http.get('/admin/get-current-time')
                .success(function(data, status, headers, config) {
                    $rootScope.startTimestamp = data.timestamp;
                    $rootScope.getMissedDiscussionsAndShowThem();
                });
        } else {
            $rootScope.getMissedDiscussionsAndShowThem();
        }
    };

    $rootScope.getMissedDiscussionsAndShowThem = function() {
        $http.get('/admin/discussions/get-next/' + $rootScope.startTimestamp)
            .success(function(data, status, headers, config) {
                var haveElements = false;
                $(data).each(function(index, element) {
                    if (($.inArray(element.id, $rootScope.queue)) !== -1) {
                        haveElements = true;
                        $rootScope.queue.append(element.id);
                        Notification.success('Новый чат №'+element.id);
                    }
                });
                if (haveElements) {
                    $rootScope.soundStartChat.play();
                }
            });
    };

    $rootScope.startConnection = function() {
        if ($scope.isChatPage) {
            function connect() {
                if ($rootScope.serverIp && !$scope.startTryingConnect && !$scope.established) {
                    $scope.startTryingConnect = true;
                    setTimeout(function() {$scope.startTryingConnect = false;}, 10000);
                    function connectSuccess(session) {
                        console.log('established');
                        $scope.connection = session;
                        if (!$scope.established) {
                            $scope.connection.subscribe('chat#' + $scope.discussionId, $scope.onEvent);
                            $scope.connection.subscribe('service_chat#' + $scope.discussionId, $scope.onServiceEvent);
                        }
                        $scope.established = true;
                        $scope.startTryingConnect = false;
                        $scope.startDiscussion();
                    }
                    function connectFailure(failure) {
                        console.log(failure);
                        if (!$scope.established) {
                            try {
                                $scope.connection.unsubscribe('chat#' + $scope.discussionId);
                                $scope.connection.unsubscribe('service_chat#' + $scope.discussionId);
                            } catch (e) {}
                            try {
                                $scope.connection = undefined;
                            } catch (e) {}
                        }
                        $scope.established = false;
                        $scope.startTryingConnect = false;
                        $scope.stopDiscussion();
                        setTimeout($rootScope.startConnection, 2000);
                    }
                    ab.connect(
                        'ws://'+$rootScope.serverIp+':81?discussionId=' + $scope.discussionId + '&type=operator',
                        connectSuccess,
                        connectFailure,
                        {
                            'maxRetries': 0,
                            'retryDelay': 0
                        }
                    );
                }
                else {
                    setTimeout($rootScope.startConnection, 1000);
                }
            }
            connect();
        }
    };

    $rootScope.startDiscussion = function() {
        $http.post('/admin/start-discussion/' + $scope.discussionId, {});

    };

    $rootScope.stopDiscussion = function() {
        $http.post('/admin/stop-discussion/' + $scope.discussionId, {});
        if ($scope.connection) {
            $scope.connection.unsubscribe('chat#' + $scope.discussionId, $scope.onEvent);
            $scope.connection.unsubscribe('service_chat#' + $scope.discussionId, $scope.onServiceEvent);
            $scope.connection.close();
        }
    };

    $rootScope.stopNotification = function() {
        if ($scope.connectionOperatorNotification) {
            $scope.connectionOperatorNotification.unsubscribe('operatorNotification', $scope.onNewChatEvent);
            $scope.connectionOperatorNotification.close();
        }
    };

    $scope.sendMessage = function(text) {
        if ($scope.established) {
            if (text) {
                var response = {
                    'text': text,
                    'title': $scope.username,
                    'avatar': $scope.avatar,
                    'type': 'response'
                };
                $scope.connection.publish('chat#'+ $scope.discussionId, JSON.stringify(response));
            }
        } else {
            setTimeout(function() {
                console.log('wait');
                $scope.sendMessage(text);
            }, 1000);
        }
    };

    $scope.sendInvoice = function(invoice) {
        if (invoice) {
            if ($scope.client.email) {
                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                $http.post('/admin/send-email', $scope.toParams({'subject':'Счет на оплату товара с burovsego.ru', 'message': 'Счет на оплату товара с burovsego.ru', 'email': $scope.client.email}))
                    .error(function(data, status, headers, config) {
                        alert('Сообщение с копией счета не было отправлено на электронную почту клиента!');
                    });
            }

            function wait() {
                if ($scope.established){
                    $scope.connection.publish('chat#'+$scope.discussionId, JSON.stringify(element));
                    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                    $http.post('/admin/messages/' + $scope.discussionId, $scope.toParams({'message': element.text, 'userId': $scope.userId}))
                        .error(function(data, status, headers, config) {
                            alert('Сообщение не отправлено!');
                        });
                }
                else {
                    setTimeout(function() {
                        wait();
                    }, 1000);
                }
            }
            var element = {
                'text': 'Сумма вашего заказа составит ' + invoice + ' рублей. Вот ссылка для оплаты заказа.',
                'type': 'order',
                'title': $scope.username,
                'avatar': $scope.avatar,
                'sum': invoice };
            $scope.chatMessage = '';
            wait();
        }
    };

    $scope.sendContactForm = function() {
        function wait() {
            if ($scope.established){
                $scope.connection.publish('chat#'+$scope.discussionId, JSON.stringify(element));
                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                $http.post('/admin/messages/' + $scope.discussionId, $scope.toParams({'message': element.text, 'userId': $scope.userId}))
                    .error(function(data, status, headers, config) {
                        alert('Сообщение не отправлено!');
                    });
            }
            else {
                setTimeout(function() {
                    wait();
                }, 1000);
            }
        }
        var element = {
            'text': 'Форма контактных данных оптравлена',
            'type': 'contact-form',
            'avatar': $scope.avatar,
        };
        wait();

    };

    $rootScope.subscribeToOperatorNotification();
}]);

BureauAdmin.directive('ngChatButton', ['$http', '$timeout', function($http, $timeout){
    function link(scope, element, attrs) {
        scope.discussionId = attrs.discussionId;
        scope.userId = attrs.userId;
        scope.avatar = attrs.ngAvatar;
        scope.username = attrs.ngUsername;
        scope.startConnection();

        element.on('click', function() {
            var message = scope.message;
            scope.message = '';
            scope.sendMessage(message);
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $http.post('/admin/messages/' + scope.discussionId, scope.toParams({'message':message, 'userId': scope.userId}))
                .error(function(data, status, headers, config) {
                    alert('Сообщение не отправлено!');
                });
        });
    }

    return {
        restrict: 'A',
        link: link
    };
}]);


BureauAdmin.directive('ngContactFormButton', ['$http', '$timeout', function($http, $timeout) {
    function link(scope, element, attrs) {
        element.on('click', function() {
            scope.sendContactForm();
        });
    }

    return {
        link: link
    };
}]);

BureauAdmin.directive('ngInvoiceButton', ['$http', '$timeout', function($http, $timeout) {
    function link(scope, element, attrs) {
        element.on('click', function() {
            var invoice = scope.invoice;
            scope.sendInvoice(invoice);
            scope.invoice = '';
            scope.invoice = 0;
        });
    }

    return {
        link: link
    };
}]);

BureauAdmin.directive('ngNotEmpty', [function() {
    function link(scope, element, attributes, ctrl) {
        var modelName = ctrl.$name;
        scope.$watch(modelName, function() {
            doValidation();
        });

        function doValidation() {
            if (!!$(element).val()) {
                ctrl.$setValidity('filled', true);
            } else {
                ctrl.$setValidity('filled', false);
            }
        }

        setTimeout(function() {
            ctrl.$setValidity('filled', true);
        }, 500);
    }

    return {
        require: 'ngModel',
        link: link,
    };
}]);


BureauAdmin.directive('ngRussianPhoneFormat', [function() {
    function link(scope, element, attributes) {
        scope.phone = '+7';
    }

    return {
        link: link,
    };
}]);

BureauAdmin.directive('ngSendActivity', ['$http', '$interval', function($http, $interval) {
    function link(scope, element, attributes, ctrl) {
        $interval(sendActivity, 1000*60*1);

        function sendActivity() {
            $http.post('/ping/');
        }

        sendActivity();
    }

    return {
        link: link,
    };
}]);


BureauAdmin.directive('ngSoundStartChat',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.soundStartChat = element[0];
        }
    }
});

BureauAdmin.directive('ngSoundMessageChat',function($rootScope){
    return {
        link:function(scope, element, attributes, ctrl) {
            $rootScope.soundChatMessage = element[0];
        }
    }
});