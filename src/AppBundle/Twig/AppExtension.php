<?php

namespace AppBundle\Twig;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\Container;

class AppExtension extends \Twig_Extension
{
    protected $doctrine;
    protected $container;
    protected $snippets;

    public function __construct(Registry $doctrine, Container $container)
    {
        $this->doctrine = $doctrine;
        $this->container = $container;
        $this->snippets = $this->doctrine->getRepository('AppBundle:Snippet')->findAll();
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
            new \Twig_SimpleFilter('base64', array($this, 'getEncodedByBase64')),
        );
    }

    public function getFunctions()
    {

        return array(
            new \Twig_SimpleFunction('newChats', array($this, 'newChats')),
            new \Twig_SimpleFunction('snippet', array($this, 'getSnippetContent', ['is_safe' => ['all']])),
            new \Twig_SimpleFunction('getTopMenuItems', array($this, 'getTopMenuItems')),
            new \Twig_SimpleFunction('getBottomMenuItems', array($this, 'getBottomMenuItems')),
            new \Twig_SimpleFunction('getBottomMenuItemsBase64', array($this, 'getBottomMenuItemsBase64')),
            new \Twig_SimpleFunction('getMetaTags', array($this, 'getMetaTags')),
            new \Twig_SimpleFunction('getHeaderHtmlBlocks', array($this, 'getHeaderHtmlBlocks')),
            new \Twig_SimpleFunction('getBodyHtmlBlocks', array($this, 'getBodyHtmlBlocks')),
        );
    }

    public function getEncodedByBase64($text)
    {
        return base64_encode($text);
    }

    public function getSnippetContent($key)
    {
        foreach ($this->snippets as $snippet) {
            if ($snippet->getName() === $key) {
                return $snippet->getContext();
            };
        }

        return "Добавьте сниппет '{$key}'";
    }

    public function getMetaTags($path)
    {
        $items = $this->doctrine->getManager()->getRepository('AppBundle:MetaTags')->findBy(['path' => $path]);

        return $items;
    }

    public function getTopMenuItems()
    {
        $items = $this->doctrine->getManager()->getRepository('AppBundle:MenuItem')->getTopMenuItems();

        return $items;
    }

    public function getBottomMenuItems()
    {
        $items = $this->doctrine->getManager()->getRepository('AppBundle:MenuItem')->getBottomMenuItems();

        return $items;
    }

    public function getHeaderHtmlBlocks($path)
    {
        $items = $this->doctrine->getRepository('AppBundle:HtmlBlock')->getHtmlBlocks($path, 'header');

        return $items;
    }

    public function getBodyHtmlBlocks($path)
    {
        $items = $this->doctrine->getRepository('AppBundle:HtmlBlock')->getHtmlBlocks($path, 'body');

        return $items;
    }

    public function getBottomMenuItemsBase64()
    {
        $items = $this->doctrine->getManager()->getRepository('AppBundle:MenuItem')->getBottomMenuItems();
        $json = json_encode($items);
        $base64 = base64_encode($json);

        return $base64;
    }

    public function newChats()
    {
        $numberOfNewChats = $this->doctrine->getManager()->getRepository('AppBundle:Discussion')->getNumberOfNewDiscussions();

        return $numberOfNewChats;
    }

    public function priceFilter($number)
    {
        $numberReversed = chunk_split(strrev($number), 3, ' ');

        return strrev($numberReversed);
    }

    public function getName()
    {
        return 'app_extension';
    }
}