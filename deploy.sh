#!/bin/bash
# ------------------------------------------------------------------
git pull
rm -R app/cache/prod/*
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs web/uploads web/media
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs web/uploads web/media
php app/console doctrine:schema:update --force
php app/console assetic:dump --env=prod --no-debug
